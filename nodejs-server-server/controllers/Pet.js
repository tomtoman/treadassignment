'use strict';

var utils = require('../utils/writer.js');
var Pet = require('../service/PetService');

module.exports.getAllImages = function getAllImages (req, res, next) {
  Pet.getAllImages()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMyImages = function getMyImages (req, res, next) {
  var userId = req.swagger.params['userId'].value;
  Pet.getMyImages(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.imageAdd = function imageAdd (req, res, next) {
  var userId = req.swagger.params['userId'].value;
  var url = req.swagger.params['url'].value;
  Pet.imageAdd(userId,url)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.imageDel = function imageDel (req, res, next) {
  var userId = req.swagger.params['userId'].value;
  var url = req.swagger.params['url'].value;
  Pet.imageDel(userId,url)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
