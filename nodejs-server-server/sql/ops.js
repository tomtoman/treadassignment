
const models = require('./models');
const Image = models.Image;

exports.ImageServerGallery = ()=>{

  return Image.findAll({where: null});
}

exports.ImageMineGallery = (githubId)=>{

  return Image.findAll({where: {githubId: githubId}});
}

exports.ImageCreate = (githubId, url)=>{

  return Image.create({githubId: githubId, url: url});
}

exports.ImageDelete = (githubId, url)=>{

  return Image.destroy({where: { githubId: githubId, url: url}});
}
