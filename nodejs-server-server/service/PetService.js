'use strict';

var ops = require('../sql/ops')

/**
 * Finds Pets by status
 * Multiple status values can be provided with comma separated strings
 *
 * no response value expected for this operation
 **/

exports.getAllImages = function() {
  return new Promise(function(resolve, reject) {

    async function afn(){
      var jsonHere = await ops.ImageServerGallery();
      resolve({imgs:jsonHere});
    }

    afn();
  });
}

/**
 * Finds Pets by status
 * Multiple status values can be provided with comma separated strings
 *
 * userId String user id
 * no response value expected for this operation
 **/
exports.getMyImages = function(userId) {
  return new Promise(function(resolve, reject) {

    async function afn(){
      var jsonHere = await ops.ImageMineGallery(userId);
      resolve({imgs:jsonHere});
    }

    afn();
  });
}

/**
 * Finds Pets by status
 * Multiple status values can be provided with comma separated strings
 *
 * userId String user id
 * url String img url
 * no response value expected for this operation
 **/
exports.imageAdd = function(userId,url) {
  return new Promise(function(resolve, reject) {

    async function afn(){
      var jsonHere = await ops.ImageCreate(userId, url)
      console.log(jsonHere);
      resolve();
    }

    afn();
  });
}

/**
 * Finds Pets by status
 * Multiple status values can be provided with comma separated strings
 *
 * userId String user id
 * url String img url
 * no response value expected for this operation
 **/

exports.imageDel = function(userId,url) {
  return new Promise(function(resolve, reject) {

    async function afn(){
      var jsonHere = await ops.ImageDelete(userId, url);
      resolve();
    }

    afn();
  });
}
