/*  EXPRESS SETUP  */

const express = require('express');
const app = express();

app.get('/login', (req, res) => res.sendFile('auth.html', { root : __dirname}));

const port = process.env.PORT || 8001;
app.listen(port , () => console.log('app listening on port ' + port));

/*  PASSPORT SETUP  */
//----------------------------------------------------------------
var session = require("express-session");
var bodyParser = require("body-parser");

app.use(express.static("public"));
app.use(session({ secret: "1234567" }));
//----------------------------------------------------------------
const passport = require('passport');
app.use(passport.initialize());
app.use(passport.session());

app.get(
  '/success',
  (req, res) =>{
    res.send('local success route');
  }
);

app.get(
  '/error',
  (req, res) =>{
    res.send("local error route");
  }
);

passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});

/*  GITHUB AUTH  */

const GitHubStrategy = require('passport-github').Strategy;

const GITHUB_CLIENT_ID = "3be9a289b2ca45a6d47c"
const GITHUB_CLIENT_SECRET = "f96ec647222e8b27204cca56de9c4adcb6c8b3b6";

passport.use(new GitHubStrategy({
    clientID: GITHUB_CLIENT_ID,
    clientSecret: GITHUB_CLIENT_SECRET,
    callbackURL: "http://54.202.53.80:8001/auth/github/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    return cb(null, profile);
  }
));

app.get(
  '/auth/github',
  passport.authenticate('github')
);

app.get(
  '/auth/github/callback',
  passport.authenticate('github', { failureRedirect: '/error' }),
  function(req, res) {
    res.redirect("http://54.202.53.80:3000");
  }
);
//----------------------------------------------------------------
var urlParse = require('url-parse');
var redirectHost = 'http://35.163.184.236:2001';

var querystring = require('querystring')
var request = require("request");

const withUserIdRouter = (req, res)=>{

  console.log(req.user);

  if(req.user!=null){
    var urlObj = urlParse(req.url, true);
    urlObj.query.userId = req.user.id;
    var options = {
      method: 'GET',
      url: redirectHost+urlObj.pathname,
      qs: urlObj.query
    };

    console.log(options);

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      res.send(body);
    });
  }
  else{
    res.status(401);
    res.send('Please login');
  }
}

const withoutUserIdRouter = (req, res)=>{

  console.log(req.user);

  var urlObj = urlParse(req.url, true);
  var options = {
    method: 'GET',
    url: redirectHost+urlObj.pathname,
    qs: urlObj.query
  };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.send(body);
  });
}

app.get('/ttt', (req, res)=>{
  console.log(req.user);
  res.send(req.user.id)
});

app.get('/imageAdd', (req, res)=>{withUserIdRouter(req, res)});
app.get('/imageDel', (req, res)=>{withUserIdRouter(req, res)});
app.get('/getMyImages', (req, res)=>{withUserIdRouter(req, res)});

app.get('/getAllImages', (req, res)=>{withoutUserIdRouter(req, res)});

app.get('/getUserInfo', (req, res)=>{
  console.log('route get userinfo')
  console.log(req.user);
  if(req.user!=null){
    res.send(
      {
        userId: req.user.id,
        username: req.user.username
      }
    );
  }
  else{
    res.status(401);
    res.send('Please login');
  }
})
